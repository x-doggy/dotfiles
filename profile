#
# ~/.profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

#Set our umask
umask 022

# Set our default path
export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/core_perl:~/.npm-global/bin:~/.AndroidStudioSdk:~/.AndroidStudioSdk/platform-tools:/opt/flutter/bin"

# no_proxy issue for flutter
export NO_PROXY=127.0.0.1

# Termcap is outdated, old, and crusty, kill it.
unset TERMCAP

# Man is much better than us at figuring this out
unset MANPATH

[ -r ~/.bashrc ] && source ~/.bashrc
