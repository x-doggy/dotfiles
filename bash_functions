screencast() {
    echo "==================  S T A R T I N G      =================="
    echo "==================    F F M P E G  . . . =================="
    ffmpeg -f x11grab -draw_mouse 1 -framerate 30 -video_size 1366x768 -i :0.0 -f pulse -name "ffmpeg screencast" -i alsa_input.pci-0000_00_1b.0.analog-stereo -pix_fmt yuv420p -c:v libx264 -preset veryfast -c:a aac -q:v 1 -s 1366x768 -f mp4 "$1".mp4
}

make480pvideo() {
    mp4aactomp4 "$1"
}

wavtomp3() {
    if [[ -s "$1.wav" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".wav -vn -ar 44100 -ac 2 -b:a 192k "$1".mp3
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

mp4aactomp4() {
    if [[ -s "$1.mp4" && -s "$1.aac" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".mp4 -i "$1".aac -c:v copy -c:a aac -strict experimental "$1_".mp4
        echo "Deleting source files..."
        rm "$1".mp4 "$1".aac
        echo "Renaming to normal name..."
        mv "$1_".mp4 "$1".mp4
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

flvtomp4() {
    if [[ -s "$1.flv" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".flv -c:v libx264 -crf 23 -c:a aac -q:a 100 "$1".mp4
        # echo "Deleting source files..."
        # rm "$1".flv
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

mp4opustomp4() {
    if [[ -s "$1.mp4" && -s "$1.opus" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".mp4 -i "$1".opus -c:v copy -c:a aac -strict experimental "$1_".mp4
        echo "Deleting source files..."
        rm "$1".mp4 "$1".opus
        echo "Renaming to normal name..."
        mv "$1_".mp4 "$1".mp4
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

threegptomp4() {
    if [[ -s "$1.3gp" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".3gp -qscale 0 -acodec copy "$1".mp4
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

mp3togg() {
    if [[ -s "$1.mp3" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -y -i "$1".mp3 -vn -acodec libvorbis "$1".ogg
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

aactomp3() {
    if [[ -s "$1.aac" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -y -i "$1".aac -acodec libmp3lame -ac 2 -ab 320k "$1".mp3
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

opustomp3() {
    if [[ -s "$1.opus" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -y -i "$1".opus -acodec libmp3lame "$1".mp3
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

oggtomp3() {
    if [[ -s "$1.ogg" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -y -i "$1".ogg -acodec libmp3lame "$1".mp3
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

modtompg() {
    if [[ -s "$1.MOD" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".MOD -vcodec copy "$1".mpg
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

m4atomp3() {
    if [[ -s "$1.m4a" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".m4a -vn -acodec libmp3lame -ac 2 -ab 320k "$1".mp3
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

mp4m4atomp4() {
    if [[ -s "$1.mp4" && -s "$1.m4a" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".mp4 -i "$1".m4a -strict experimental "$1_".mp4
        echo "Deleting source files..."
        rm "$1".mp4 "$1".m4a
        echo "Renaming to normal name..."
        mv "$1_".mp4 "$1".mp4
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

flactomp3() {
    if [[ -s "$1.flac" ]] ;
    then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".flac -ab 320k -map_metadata 0 -id3v2_version 3 "$1".mp3
        echo "OK"
    else
        >&2 echo "Something's wrong with FFMPEG!"
    fi
}

webmtomp3 () {
  if [[ -s "$1.webm" ]] ;
  then
        echo "==================  S T A R T I N G      =================="
        echo "==================    F F M P E G  . . . =================="
        ffmpeg -i "$1".webm -vn -ab 128k -ar 44100 -y "$1".mp3
        echo "OK"
  else
    >&2 "Something's wrong with FFMPEG!"
  fi
}

calc() {
    echo "scale=3;$@" | bc -l
}

pkglist() {
    if [ "$1" ] ;
    then
        pacman -Qqe > $1
    else
        pacman -Qqe
    fi
}

cheatsheet() {
    echo "
┌─────────────────────────────────────────┬─────────────────┐
│   Описание                              │   Сочетание     │
├─────────────────────────────────────────┼─────────────────┤
│ Начало строки                           │ Ctrl-a          │
│ Конец строки                            │ Ctrl-e          │
│ Один символ вперед                      │ Ctrl-f          │
│ Один символ назад                       │ Ctrl-b          │
│ Одно слово вперёд                       │ Alt -f          │
│ Одно слово назад                        │ Alt -b          │
│ Удалить символ под курсором             │ Ctrl-d          │
│ Удалить символ перед курсором           │ Ctrl-h          │
│ Предыдущая команда в истории            │ Ctrl-p          │
│ Следующая команда в истории             │ Ctrl-n          │
│ Ввод команды                            │ Ctrl-j (Ctrl-m) │
│ Вырезать предыдущее слово               │ Ctrl-w          │
│ Вырезать следующее слово                │ Alt -d          │
│ Вырезать все к концу строки             │ Ctrl-k          │
│ Вырезать все к началу строки            │ Ctrl-u          │
│ Вставить вырезанное ранее               │ Ctrl-y          │
│ Удалить слово до разделителя            │ Alt -BkSp       │
│ Удалить комментарий в начале строки     │ Alt -Shift-3    │
│ Переставить 2 слова слева               │ Alt -t          │
│ ЗАЯЦ -> заяц                            │ Alt -L          │
│ Откатить изменение                      │ Ctrl-_          │
│ Очистить экран                          │ Ctrl-l          │
│ Скроллить вверх/вниз                    │ Shift-PgUp/PgDn │
│ Интерактивный поиск по истории команд   │ Ctrl-r          │
│ Выход из поиска                         │ Ctrl-g          │
│ Выход без очистки строки                │ Ctrl-o          │
│ Написать команду в \$EDITOR              │ Ctrl-x+e        │
│ bash --version                          │ Ctrl-x+v        │
└─────────────────────────────────────────┴─────────────────┘"
}

onesignal() {
    local FIRST="$1"
    if [[ "$1" && "$2" && "$3" && "$4" ]]; then
        curl --include --request POST --header "Content-Type: application/json; charset=utf-8" --header "Authorization: Basic Nzc4YWI5YjAtZTFjYi00ZDkwLTliN2EtMmZjYWJkYTU1NTY5" --data-binary "{\"app_id\": \"ddca1f15-0821-4244-bdd4-0caaf4a85a1d\", \"contents\": {\"en\": \"$4\"}, \"data\": {\"type\": \"recommendation\", \"priority\": \"$1\", \"subtype\": \"$2\", \"deviceId\": \"$3\"}, \"include_player_ids\": [\"b432acd5-77a5-404e-b868-d0a61b5394e2\"]}" https://onesignal.com/api/v1/notifications
    fi
}
