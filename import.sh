#!/bin/bash

#
# +------------------------------------------------------+
# | (c) 15.05.2018, Vladimir Stadnik, <x-doggy at ya.ru> |
# +------------------------------------------------------+
#

OS="Linux"

check_utils() {
  local needed_prgs=(
    "cat"
    "yes"
    "cp"
    "grep"
    "awk"
    "bash"
  )
  local ok=1
  for i in "${needed_prgs[@]}" ; do
    echo "Check if $i exists..."
    if [ -x `command -v $i` ] ; then
      echo "[OK]        $i"
    else
      echo "[NOT FOUND] $i"
      ok=0
    fi
  done
  if [[ $ok -eq 0 ]] ; then
    echo "Please install necessary dependencies before continue" 1>&2
    exit 1
  fi
}

check_os() {
  OS="`cat /etc/*-release | grep ^NAME`"
  OS=${OS:6:-1}
  echo "DETECTED OS $OS"
}



#
# +----------------------------------+
# | COPY CONFIG FILES TO CURRENT DIR |
# +----------------------------------+
#

apply_dotfiles() {
  local files_home_config=(
    "bash_aliases"
    "bash_functions"
    "bashrc"
    "profile"
    "emacs"
    "gitconfig"
    "screenrc"
    "vimrc"
  )
  local files_system_config=(
    "/etc/locale.conf"
    "/etc/vconsole.conf"
    "/etc/os-release"
    "/etc/default/grub"
    "/etc/sysctl.conf"
    "/etc/pacman.conf"
  )
  declare -A files_specific
  files_specific[yay.json]="/home/$USER/.config/yay/config.json"

  # Apply home config files
  for i in "${files_home_config[@]}" ; do
    apply_home_config "$i"
  done

  # Apply system config files
  for i in "${files_system_config[@]}" ; do
    apply_file "$i"
  done

  # Apply specific files
  for i in "${!files_specific[@]}" ; do
    apply_specific "${files_specific[$i]}" "$i"
  done

  apply_php
}

echo_ok_log() {
  echo File "$1" applied into "$2"
}

apply_home_config() {
  yes | cp ~/."$1" "$1"
  echo_ok_log ~/."$1" "$1"
}

apply_file() {
  local arg="$1"
  local cut_arg="${arg##*/}"
  # Check if file exists and readable
  if test -r "$arg" -a -f "$arg" ; then
    cat $arg > $cut_arg
  else
    echo "File $arg is not readable" 1>&2
  fi
  echo_ok_log $arg $cut_arg
}

apply_specific() {
  yes | cp "$1" "$2"
  echo_ok_log "$1" "$2"
}

apply_php() {
  if [ -x "`which php`" ] ; then
    apply_file $(php -r "phpinfo();" | grep "Loaded Configuration File" | awk '{ print $5 }');
  else
    echo "No PHP in your system!";
  fi
}



check_os
check_utils
apply_dotfiles

