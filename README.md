# Dot files

#### Vundle for Vim installation

Original manual is posted here: [https://github.com/VundleVim/Vundle.vim](https://github.com/VundleVim/Vundle.vim).

``.vimrc`` is already configured for Vundle use. Left to do simple instruction:

```
mkdir -p .vim/bundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

To install plugins launch `vim` and run `:PluginInstall`

To install from command line:

```
vim +PluginInstall +qall
```
